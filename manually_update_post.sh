#!/bin/zsh

if [ $# -gt 2 ]; then
  Y=$1
  M=$2
  D=$3
else
  Y=$(date +%Y)
  M=$(date +%m)
  D=$(date +%d)
fi

REMOTE_ROOT_DIR=/Volumes/Tibor.Stanko
SITE_DIR=_site
POST_CATEGORY=surfsketch
# POST_CATEGORY=vr-sketching

bundle exec jekyll build

if [ -d $REMOTE_ROOT_DIR ]; then
  echo "$REMOTE_ROOT_DIR mounted: Ok"

  FOLDER_TO_COPY=$SITE_DIR/$POST_CATEGORY/$Y/$M/$D
  REMOTE_FULL_DIR=$REMOTE_ROOT_DIR/$FOLDER_TO_COPY

  echo $REMOTE_FULL_DIR

  # create remote dir if needed
  mkdir -p $REMOTE_FULL_DIR

  echo "copy $FOLDER_TO_COPY to remote dir"
  cp -r $FOLDER_TO_COPY/. $REMOTE_FULL_DIR

  echo "copy index.html to remote dir"
  cp $SITE_DIR/index.html $REMOTE_ROOT_DIR/$SITE_DIR/index.html

else
  echo "$REMOTE_ROOT_DIR mounted: No"
  echo "abort"
fi
