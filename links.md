---
layout: page
title: Links
permalink: /links.html
---
* Inria gitlab: [Surfacing Unstructured 3D Curve Networks](https://gitlab.inria.fr/surfsketch)
* [OpenFlipper gitlab](https://graphics.rwth-aachen.de:9000/OpenFlipper-Free/OpenFlipper-Free)
