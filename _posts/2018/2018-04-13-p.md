---
layout: post
title: "Snapping only one coordinate to grid"
date: 2018-04-13 00:00:00 +0100
category: igsv
---
- snapping only one coordinate to grid
- snap both if the constraint direction is not clear (around junctions)
- fitted Bezier curves, trying to implement C0 constraints

{% screenshot --group="2018-04-13" --date="2018-04-13" --time="16.52.30" %}
{% screenshot --group="2018-04-13" --date="2018-04-13" --time="16.52.33" %}
{% screenshot --group="2018-04-13" --date="2018-04-13" --time="16.52.46" %}
{% screenshot --group="2018-04-13" --date="2018-04-13" --time="17.24.16" %}
{% screenshot --group="2018-04-13" --date="2018-04-13" --time="17.24.23" %}
