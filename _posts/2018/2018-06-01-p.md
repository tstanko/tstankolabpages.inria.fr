---
layout: post
title: "Parametrization"
date: 2018-06-01 00:00:00 +0100
category: igsv
---
- fitting penalty=0.1
- transition functions penalty=10 (+local Gaussian distance weighting)
- optimization took 27'11''

{% screenshot --group="2018-06-01" --date="2018-06-01" --time="09.12.07" %}
{% screenshot --group="2018-06-01" --date="2018-06-01" --time="09.14.35" %}
{% screenshot --group="2018-06-01" --date="2018-06-01" --time="09.39.00" %}

- varying scale

{% screenshot --group="2018-06-01" --date="2018-06-01" --time="13.45.01" %}
{% screenshot --group="2018-06-01" --date="2018-06-01" --time="13.45.02" %}
{% screenshot --group="2018-06-01" --date="2018-06-01" --time="13.45.03" %}
{% screenshot --group="2018-06-01" --date="2018-06-01" --time="13.45.04" %}
