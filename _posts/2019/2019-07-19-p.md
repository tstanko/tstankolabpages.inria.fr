---
layout: post
title: "Experiments: mesh density"
date: 2019-07-19 00:00:00 +0100
category: surfsketch
---

- in general, higher mesh density --> more linearization artifacts (field tends to get 'more constant')

## 20 input points per curve

{% comment %} 2019-07-19 at 09:45:17 {% endcomment %}
### simple_parabolic
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.29.32" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.29.41" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.30.01" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.30.27" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.31.24" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.31.26" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.35.42" %}

### simple_wave_1
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.36.09" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.36.36" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.37.37" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.38.30" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.38.51" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.39.45" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.39.47" %}

### simple_wave_2
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.41.47" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.41.49" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.42.39" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.42.40" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.42.58" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.44.10" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.44.12" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.44.26" %}

## simple_elliptic
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.25.29" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.25.44" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.26.14" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.27.06" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.28.49" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.28.50" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.29.16" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.29.47" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.30.48" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.30.52" %}

## 40 input points per curve
{% comment %} 2019-07-19 at 10:05:51 {% endcomment %}

### simple_parabolic
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.55.05" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.55.07" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.57.07" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.57.09" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.57.46" %}

### simple_wave_2
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.58.22" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.58.26" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="09.59.37" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.00.06" %}

## 80 input points per curve
### simple_wave_2
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.00.49" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.00.53" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.04.24" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="10.04.26" %}

## misc
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="13.37.37" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="13.37.42" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.28.13" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.28.46" %}

{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.07.34" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.07.44" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.08.05" %}

{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.24.53" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.25.13" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.25.44" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.26.14" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.26.37" %}

## Barycentric interpolation: original SH coeffs (before projection)
{% comment %} 2019-07-19 at 14:51:26 {% endcomment %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.49.44" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.49.45" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.50.44" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.50.51" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.50.53" %}

{% comment %} 2019-07-19 at 14:53:54 {% endcomment %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.03" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.08" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.13" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.18" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.31" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.37" %}
{% screenshot --comment="" --group="2019-07-19" --date="2019-07-19" --time="14.53.47" %}
