---
layout: post
title: "Current extraction results"
date: 2019-01-04 00:00:00 +0100
category: igsv
---

## Puppy (WITH short curve removal)
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="09.59.32" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="09.59.37" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.00.24" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.00.28" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.00.35" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.00.45" %}

{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.18.35" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.18.39" %}

## Cloud
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.41.55" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.41.59" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.42.04" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="14.42.06" %}

## Archi
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.04" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.07" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.38" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.43" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.49" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.06.53" %}

## Mecha
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.06" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.12" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.22" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.26" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.48" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.14.51" %}

## Cartoon
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.01" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.07" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.41" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.44" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.52" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.17.58" %}

## Car
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.21.01" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.21.05" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.21.15" %}
{% screenshot --group="2019-01-04" --date="2019-01-04" --time="10.21.18" %}
