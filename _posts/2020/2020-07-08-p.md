---
layout: post
title: "mesh abstractions"
date: 2020-07-08 00:00:00 +0100
category: vr-sketching
---
## airplane
- original mesh, then meshes simplified with Blender's `decimate` modifier, four different levels

{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.40.55" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.41.15" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.41.20" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.41.25" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.41.30" %}

## porsche
- Blender's `remesh` modifier, mode=block (voxelization)

{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.48.45" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.49.41" %}

## shoe
- Blender's `decimate` modifier

{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.56.12" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.56.15" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.56.30" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.56.40" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.56.47" %}

- Blender's `remesh` modifier (voxelization)

{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.59.31" %}

- remeshing via Instant meshes

{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.59.48" %}
{% screenshot --comment="" --group="2020-07-08" --date="2020-07-08" --time="11.59.57" %}
