---
layout: post
title: "2020-06-18"
date: 2020-06-18 00:00:00 +0100
category: surfsketch
---

## Further tests with CoMISo
Adjusting CoMISo parameters (# of local iters, # of CG iters), the result improves:

Results for different scales:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="11.57.37" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.12.28" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.15.24" %}

## Car example
This input is too complex for our pipeline...

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.25.58" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.07" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.30" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.39" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.42" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.46" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.28.17" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.28.20" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.28.24" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.15.29" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.17.58" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.18.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.19.54" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.20.00" %}

## Card reader -- wide band

ground truth:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.32.11" %}

sketchified curves:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.32.13" %}

surface mesh for a wide band:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.32.32" %}

skeletonization fails:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.32.17" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.32.20" %}

tangent direction of the frame field + cuts:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.38.52" %}

we have problems around the singularity:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.41.42" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.41.44" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.41.49" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.43.01" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.57.54" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.58.06" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.58.09" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.58.34" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.58.40" %}

surface mesh for a narrower band:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.10.55" %}

skeletonization still fails:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.10.46" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.30.23" %}

ours, best result -- only a few extra edges, which could be removed by only tracing in the tangent direction:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.29.02" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.30.17" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.17.04" %}

here's the tangent direction:

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.31.52" %}

ours, other results (different scale):

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.11.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.08.18" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.08.31" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.11.17" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.13.21" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.13.39" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="14.13.41" %}

## Failing inputs
### Gamepad
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.34.55" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.34.57" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.34.59" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.35.01" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.35.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.35.14" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.14.34" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.25.32" %}

## Usable inputs
### Cone
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.25.15" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.26.03" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.26.07" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.26.12" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.26.16" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.26.21" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.27.07" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="15.27.14" %}

### Modem
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.36.03" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.36.05" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.36.10" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.36.14" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.42.52" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.53.51" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.54.03" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.54.05" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.57.14" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.57.29" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.57.36" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.57.43" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="16.57.54" %}

### Ring
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.52.58" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.53.00" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.53.02" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.53.44" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.57.10" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="17.57.15" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.04.38" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.04.40" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.05.34" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.06.11" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.38.06" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.39.11" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.39.29" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.40.09" %}

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.07.52" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.08.05" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.08.29" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.08.30" %}

### Blender
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="18.42.31" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="19.00.00" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="19.46.02" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="20.26.39" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.23.52" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.33.55" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.34.25" %}
