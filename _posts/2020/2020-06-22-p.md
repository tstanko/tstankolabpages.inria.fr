---
layout: post
title: "Summary of current results"
date: 2020-06-22 00:00:00 +0100
category: surfsketch
---

## Usable results

| name | input curves | surf mesh | skeleton | ours |
|---|---|---|---|
card reader | {% screenshot -d="2020-06-22" -t="10.42.46" %} | {% screenshot -d="2020-06-22" -t="10.42.52" %} | {% screenshot -d="2020-06-22" -t="10.43.00" %} | {% screenshot -d="2020-06-22" -t="10.43.10" %} |
cone | {% screenshot -d="2020-06-22" -t="10.48.18" %} | {% screenshot -d="2020-06-22" -t="10.48.21" %} | {% screenshot -d="2020-06-22" -t="10.51.06" %} | {% screenshot -d="2020-06-22" -t="10.51.23" %} |
modem | {% screenshot -d="2020-06-22" -t="11.07.04" %} | {% screenshot -d="2020-06-22" -t="11.07.10" %} | {% screenshot -d="2020-06-22" -t="11.07.17" %} | {% screenshot -d="2020-06-22" -t="11.07.52" %} |
ring<br />(view 1)| {% screenshot -d="2020-06-22" -t="11.17.36" %} | {% screenshot -d="2020-06-22" -t="11.17.42" %} | {% screenshot -d="2020-06-22" -t="11.17.49" %} <br /> {% screenshot -d="2020-06-22" -t="11.18.05" %} | {% screenshot -d="2020-06-22" -t="11.18.25" %} <br /> {% screenshot -d="2020-06-22" -t="11.18.51" %} |
ring<br />(view 2) | {% screenshot -d="2020-06-22" -t="11.19.57" %} | {% screenshot -d="2020-06-22" -t="11.19.59" %} | {% screenshot -d="2020-06-22" -t="11.20.03" %}{% screenshot -d="2020-06-22" -t="11.20.06" %} | {% screenshot -d="2020-06-22" -t="11.20.11" %}{% screenshot -d="2020-06-22" -t="11.20.13" %} |
spoon<br />(view 1) | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.34.38" %}{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.34.42" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.34.46" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.34.51" %}{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.35.08" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.35.14" %}{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.35.19" %} |
spoon<br />(view 2) | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.36.45" %} <br /> {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.36.49" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.36.52" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.37.00" %} <br /> {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.37.03" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.37.08" %} <br /> {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.37.10" %} |
mug<br />(from [[Schmidt 2009]](http://www.dgp.toronto.edu/~rms/pubs/DrawingSGA09.html)) | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.36.46" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.36.51" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.37.00" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.37.08" %} |
mug<br />(close-up) | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.37.52" %} | | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.38.12" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="10.38.18" %} |
bumpy cube | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.54.55" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.54.58" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.55.04" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="11.55.07" %} |
sewing machine | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.14.10" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.14.17" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.14.26" %} | {% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.32.27" %}{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.14.35" %} |


## Other inputs from [[Schmidt 2009]](http://www.dgp.toronto.edu/~rms/pubs/DrawingSGA09.html)
### Doghead, coarser scale
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.51.38" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.51.44" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.51.59" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.52.03" %}

### Doghead, finer scale
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.55.09" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="15.55.14" %}

### Car
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="16.08.49" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="16.12.31" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="16.11.38" %}

## Failures
- box

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-08" --time="12.02.47" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="11.57.37" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.12.28" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.15.24" %}


- blender

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="21.34.25" %}

- car

{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.25.58" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="12.26.46" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.17.58" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.18.04" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.19.54" %}
{% screenshot --comment="" --group="2020-06-18" --date="2020-06-18" --time="13.20.00" %}

- curved cylinder

{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="14.14.39" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="14.14.37" %}

- vase

{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="14.38.59" %}
{% screenshot --comment="" --group="2020-06-19" --date="2020-06-19" --time="14.39.02" %}

- toothpaste

{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.42.29" %}
{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.42.32" %}
{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.42.36" %}
{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.42.49" %}


## **** new pics ****
{% screenshot --comment="" --group="2020-06-22" --date="2020-06-22" --time="14.46.47" %}
