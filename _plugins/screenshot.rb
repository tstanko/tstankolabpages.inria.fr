module Jekyll

  # ---------------------------------------------------------------------------

  class ScreenshotTag < Liquid::Tag
    def initialize(tag_name, text, token)
      super
      if text =~ /--group="([^"]*)"/i
        @group = text.match(/--group="([^"]*)"/i)[1]
      elsif text =~ /-g="([^"]*)"/i
        @group = text.match(/-g="([^"]*)"/i)[1]
      else
        @group = ''
      end
      if text =~ /--date="([^"]*)"/i
        @date = text.match(/--date="([^"]*)"/i)[1]
        @year = @date[0..3]
        @month = @date[5..6]
      elsif text =~ /-d="([^"]*)"/i
        @date = text.match(/-d="([^"]*)"/i)[1]
        @year = @date[0..3]
        @month = @date[5..6]
      else
        @date = ''
        @year = ''
        @month = ''
      end
      if text =~ /--time="([^"]*)"/i
        @time = text.match(/--time="([^"]*)"/i)[1]
      elsif text =~ /-t="([^"]*)"/i
        @time = text.match(/-t="([^"]*)"/i)[1]
      else
        @time = ''
      end
      if text =~ /--comment="([^"]*)"/i
        @comment = text.match(/--comment="([^"]*)"/i)[1]
      elsif text =~ /-c="([^"]*)"/i
        @comment = text.match(/-c="([^"]*)"/i)[1]
      else
        @comment = @date
      end

    end
    def render(context)
      if @date.empty? || @time.empty?
        # do nothing
        # %{[empty]}
      else
        filename = 'Screenshot '+@date+' at '+@time+'.png'
        thumbdir = 'https://www-sop.inria.fr/members/Tibor.Stanko/_screenshots/tiny-fs8/'+@year+'/'+@month+'/'
        fullrdir = 'https://www-sop.inria.fr/members/Tibor.Stanko/_screenshots/full-fs8/'+@year+'/'+@month+'/'
        thumburl = thumbdir+filename
        fullrurl = fullrdir+filename
        # %{<a href="#{fullrurl}" data-lightbox="#{@group}" data-title="#{@group} : #{@comment}"><img class="thumb" src="#{thumburl}" /></a>}
        %{<a href="#{fullrurl}" class="magnific-popup" title="#{@group} : #{@comment}"><img class="thumb" src="#{thumburl}" /></a>}
      end
    end
  end

  # ---------------------------------------------------------------------------

  # class Data2dTag < Liquid::Tag
  #   def initialize(tag_name, text, token)
  #     super
  #     if text =~ /--group="([^"]*)"/i
  #       @group = text.match(/--group="([^"]*)"/i)[1]
  #     else
  #       @group = ''
  #     end
  #     if text =~ /--name="([^"]*)"/i
  #       @name = text.match(/--name="([^"]*)"/i)[1]
  #     else
  #       @name = ''
  #     end
  #   end
  #   def render(context)
  #     src = '/members/Tibor.Stanko/data/2d/'+@name+'.png'
  #     %{<a href="#{src}" data-lightbox="#{@group}" data-title="#{@group}"><img class="thumb" src="#{src}"/></a>}
  #   end
  # end

  # ---------------------------------------------------------------------------

#   class ImageTag < Liquid::Tag
#     def initialize(tag_name, text, token)
#       super
#       if text =~ /--group="([^"]*)"/i
#         @group = text.match(/--group="([^"]*)"/i)[1]
#       else
#         @group = ''
#       end
#       if text =~ /--name="([^"]*)"/i
#         @name = text.match(/--name="([^"]*)"/i)[1]
#       else
#         @name = ''
#       end
#       if text =~ /--dir="([^"]*)"/i
#         @dir = text.match(/--dir="([^"]*)"/i)[1]
#       else
#         @dir = 'screenshots'
#       end
#     end
#     def render(context)
#       src = '/members/Tibor.Stanko/'+@dir+'/'+@name+'.png'
#       %{<a href="#{src}" data-lightbox="#{@group}" data-title="#{@group}"><img class="thumb" src="#{src}"/></a>}
#     end
#   end

end

# ---------------------------------------------------------------------------

Liquid::Template.register_tag('screenshot', Jekyll::ScreenshotTag)
# Liquid::Template.register_tag('data2d', Jekyll::Data2dTag)
# Liquid::Template.register_tag('image', Jekyll::ImageTag)
