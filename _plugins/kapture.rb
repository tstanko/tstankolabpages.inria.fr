module Jekyll
  class KaptureTag < Liquid::Tag
    def initialize(tag_name, text, token)
      super
      if text =~ /--group="([^"]*)"/i
        @group = text.match(/--group="([^"]*)"/i)[1]
      else
        @group = ''
      end
      if text =~ /--date="([^"]*)"/i
        @date = text.match(/--date="([^"]*)"/i)[1]
        @year = @date[0..3]
        @month = @date[5..6]
        # @year = ''
      else
        @date = ''
        @year = ''
        @month = ''
      end
      if text =~ /--time="([^"]*)"/i
        @time = text.match(/--time="([^"]*)"/i)[1]
      else
        @time = ''
      end
      if text =~ /--comment="([^"]*)"/i
        @comment = text.match(/--comment="([^"]*)"/i)[1]
      else
        @comment = @date
      end
    end
    def render(context)
      gifurl = 'https://www-sop.inria.fr/members/Tibor.Stanko/screenshots/gif/Kapture '+@date+' at '+@time+'.gif'
      %{<a href="#{gifurl}" data-lightbox="#{@group}" data-title="#{@group} : #{@comment}"><img class="thumb" src="#{gifurl}"/></a>}
    end
  end
end

Liquid::Template.register_tag('kapture', Jekyll::KaptureTag)
