#!/bin/zsh
setopt null_glob

DEFAULT_CATEGORY=surfsketch
PRINT_COMMANDS=false
TEST_RUN=false

# check that the remote dir is connected
REMOTEDIR=/Volumes/Tibor.Stanko
if [ ! -e ${REMOTE} ]; then
  echo "ERROR: remote not connected [${REMOTEDIR}]"
  return
fi

PNGQUANT=~/Utils/pngquant/pngquant
LOCAL_SCREENDIR=~/Postdoc-screenshots
REMOTE_SCREENDIR=${REMOTEDIR}/_screenshots
SITEDIR=$(pwd) # root of the website!
echo "WEBSITE ROOT = ${SITEDIR}"

# regex for processing filenames into jekyll screenshot tags
SCREENSHOT_REGEX='s/Screenshot \([0-9]\{4\}\)-\([0-9]\{2\}\)-\([0-9]\{2\}\) at \([0-9]\{2\}\)\.\([0-9]\{2\}\)\.\([0-9]\{2\}\)\.png/{% screenshot --comment="" --group="\1-\2-\3" --date="\1-\2-\3" --time="\4\.\5\.\6" %}/'

KAPTURE_REGEX='s/Kapture \([0-9]\{4\}\)-\([0-9]\{2\}\)-\([0-9]\{2\}\) at \([0-9]\{2\}\)\.\([0-9]\{2\}\)\.\([0-9]\{2\}\)\.gif/{% kapture    --comment="" --group="\1-\2-\3" --date="\1-\2-\3" --time="\4\.\5\.\6" %}/'

if [ $# -gt 2 ]; then
  Y=$1
  M=$2
  D=$3
else
  Y=$(date +%Y)
  M=$(date +%m)
  D=$(date +%d)
fi

DATE="$Y-$M-$D"
POSTF=${SITEDIR}/_posts/$Y/${DATE}-p.md

# make sure the dirs exist
mkdir -p ${REMOTE_SCREENDIR}/full-fs8/$Y/$M/
mkdir -p ${REMOTE_SCREENDIR}/tiny-fs8/$Y/$M/
mkdir -p ${REMOTE_SCREENDIR}/gif/
mkdir -p ${SITEDIR}/_posts/$Y/
mkdir -p ${LOCAL_SCREENDIR}/full/${Y}/${M}/.

# init the post file
if [ ! -e ${POSTF} ]; then
  echo "New post: ${POSTF}\n"
  cat > $POSTF <<- EOM
---
layout: post
title: "${DATE}"
date: ${DATE} 00:00:00 +0100
category: ${DEFAULT_CATEGORY}
---
EOM

else
  echo ""
  echo " WARNING :  ${POSTF}"
  echo " WARNING :  already exists"
  echo " WARNING :  appending output"
  echo ""
fi

if [ ! -e ${POSTF} ]; then
  echo ""
  echo " ERROR :  ${POSTF}"
  echo " ERROR :  Could not create the file."
  echo " ERROR :  Does the directory exist?"
  echo ""
  return
fi

cd ${LOCAL_SCREENDIR}

for IMG in Screenshot*${DATE}*.png; do

  IMG2=${REMOTE_SCREENDIR}/full-fs8/$Y/$M/$IMG
  THU2=${REMOTE_SCREENDIR}/tiny-fs8/$Y/$M/$IMG

  if [ ! -e ${IMG2} ]; then

    if [ "$PRINT_COMMANDS" = true ]; then
      echo ${IMG}
    else
      echo -n "${IMG} --> "
    fi

    ### move and quantize
    if [ "$PRINT_COMMANDS" = true ]; then
      echo "  --> quantize ... "
      echo "       [${PNGQUANT} --output $IMG2 -- $IMG]"
    fi
    echo -n "."
    if [ "$TEST_RUN" = true ]; then
      sleep .01
    else
      ${PNGQUANT} --output $IMG2 -- $IMG
    fi

    ### create the thumbnail
    if [ "$PRINT_COMMANDS" = true ]; then
      echo "  --> thumbnail ... "
      echo "       [convert $IMG2 -resize x150\> ${THU2}]"
    fi
    echo -n "."
    if [ "$TEST_RUN" = true ]; then
      sleep .01
    else
      convert $IMG2 -resize x150\> ${THU2}
    fi

    ### print entry into the file
    ENTRY="$(echo ${IMG} | sed -e ${SCREENSHOT_REGEX})"
    if [ "$PRINT_COMMANDS" = true ]; then
      echo "  --> add entry ... "
      echo "      [${ENTRY}]"
    fi
    echo -n "."
    if [ "$TEST_RUN" = true ]; then
      sleep .01
    else
      echo ${IMG} | sed -e ${SCREENSHOT_REGEX} >> ${POSTF}
    fi

    ### archive the original screenshot
    if [ "$PRINT_COMMANDS" = true ]; then
      echo "  --> archive ... "
      echo "      [mv ${IMG} full/${Y}/${M}/.]"
    fi
    echo -n "."
    if [ "$TEST_RUN" = true ]; then
      sleep .01
    else
      mv ${IMG} full/${Y}/${M}/.
    fi

    if [ "$PRINT_COMMANDS" = true ]; then
      echo "  --> done."
      echo "============================================================"
    fi

    echo " --> done."

  fi

done

for GIF in Kapture*${DATE}*.gif; do

  if [ ! -e ${REMOTE_SCREENDIR}/gif/${GIF} ]; then

    echo ${GIF}

    ### print entry into the file
    ENTRY="$(echo ${GIF} | sed -e ${KAPTURE_REGEX})"
    echo "--> add entry [${ENTRY}]"
    echo ${GIF} | sed -e ${KAPTURE_REGEX} >> ${POSTF}

    # copy to remote
    cp ${GIF} ${REMOTE_SCREENDIR}/gif/.

    ### archive the original screenshot
    echo "--> archive [mv ${GIF} gif/.]"
    mv ${GIF} gif/.

  fi

done
